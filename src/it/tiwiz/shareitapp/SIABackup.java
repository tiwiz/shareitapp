package it.tiwiz.shareitapp;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupManager;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;

public class SIABackup extends BackupAgentHelper{

    @Override
    public void onCreate(){
        SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this,StoredData.ROOT_KEY);
        addHelper(StoredData.BACKUP_KEY,helper);
    }

    //Requests backup
    public static final void requestBackup(Context mContext){

        BackupManager b = new BackupManager(mContext);
        b.dataChanged();

    }
}
