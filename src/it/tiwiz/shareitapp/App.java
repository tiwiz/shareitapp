package it.tiwiz.shareitapp;

import android.graphics.drawable.Drawable;

public class App implements Comparable<App>{
	
	private String appName, packageName, packagePath;
	private Drawable appIcon, appInstallerIcon;
	private boolean isSelected, installedFromPlayStore;
	private int alternativeSource;
	private String alternativeDetails;
	
	private final static int EQUAL = 0;
	public static final int NOT_SET = -1;
	public static final int PLAY_STORE = 0;
	public static final int AMAZON_APP_SHOP = 1;
	public static final int XDA_DEVELOPERS = 2;
	public static final int OTHER = 3;
	
	public App(String appName, String packageName, Drawable appIcon){
		this.appName = appName;
		this.packageName = packageName;
		this.appIcon = appIcon;
		this.isSelected = false;
		this.installedFromPlayStore = true;
		this.packagePath = "";
		this.alternativeSource = App.NOT_SET;
		this.alternativeDetails = "";
	}

	public Drawable getAppInstallerIcon() {
		return appInstallerIcon;
	}

	public void setAppInstallerIcon(Drawable appInstallerIcon) {
		this.appInstallerIcon = appInstallerIcon;
	}

	public boolean isInstalledFromPlayStore() {
		return installedFromPlayStore;
	}

	public void setInstalledFromPlayStore(boolean installedFromPlayStore) {
		this.installedFromPlayStore = installedFromPlayStore;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public Drawable getAppIcon() {
		return appIcon;
	}

	public void setAppIcon(Drawable appIcon) {
		this.appIcon = appIcon;
	}
	
	public void setSelected(boolean isSelected){
		this.isSelected = isSelected;
	}

	public boolean getSelected(){
		return isSelected;
	}

	public String getPackagePath() {
		return packagePath;
	}

	public void setPackagePath(String packagePath) {
		this.packagePath = packagePath;
	}

	@Override
	public int compareTo(App another) {
		
		String anotherName = another.getAppName();
		String anotherPackage = another.getPackageName();
		
		
		if(this.packageName.equalsIgnoreCase(anotherPackage))
			return EQUAL;
		
		int nameResult = this.appName.compareToIgnoreCase(anotherName);
		
		if(nameResult != 0) return nameResult;
		else
			return this.packageName.compareToIgnoreCase(anotherPackage);
	}

	public int getAlternativeSource() {
		return alternativeSource;
	}

	public void setAlternativeSource(int alternativeSource) {
		this.alternativeSource = alternativeSource;
	}

	public String getAlternativeDetails() {
		return alternativeDetails;
	}

	public void setAlternativeDetails(String alternativeDetails) {
		this.alternativeDetails = alternativeDetails;
	}

	
}
