package it.tiwiz.shareitapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;

public class PMFilter {

	public static List<App> getInstalledApps(Context mContext){
		ArrayList<App> apps = new ArrayList<App>();
		
		PackageManager pm = mContext.getPackageManager();
		List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		
		for(ApplicationInfo appInfo : packages){
			
			if((appInfo.flags&ApplicationInfo.FLAG_SYSTEM)==0){
				Drawable icon = pm.getApplicationIcon(appInfo);
				String appName = (String) appInfo.loadLabel(pm);
				String packageName = appInfo.packageName;
				App tmp = new App(appName,packageName,icon);
				tmp.setPackagePath(appInfo.sourceDir);
				Installer tmpInstaller = StoredData.getInstaller(mContext, packageName); //looks for alternative sources
				int type = tmpInstaller.getType();
				if(type != App.NOT_SET){ //if user alread gave an alternative source
					tmp.setAlternativeSource(type);
					tmp.setAlternativeDetails(tmpInstaller.getDetails());
					tmp.setAppInstallerIcon(getInstallerDrawable(mContext,type));
				}else{ //otherwise we search for standard
					try {
						@SuppressWarnings("unused")
						ApplicationInfo installer = pm.getApplicationInfo(pm.getInstallerPackageName(packageName), 0); //gets the installer of the package
						tmp.setAppInstallerIcon(getInstallerDrawable(mContext,App.PLAY_STORE));
					} catch (NameNotFoundException e) {
						tmp.setInstalledFromPlayStore(false);
						tmp.setAppInstallerIcon(getInstallerDrawable(mContext,App.NOT_SET));
					} 
				}
				apps.add(tmp);
			}
			
		}
		return apps;
		
	}
	
	private static Drawable getInstallerDrawable(Context mContext, int source){
		
		int iconId;
		
		switch(source){
		case App.NOT_SET:
			iconId = R.drawable.icon_not_sure;
			break;
		case App.PLAY_STORE:
			iconId = R.drawable.btn_play_store;
			break;
		case App.AMAZON_APP_SHOP:
			iconId = R.drawable.btn_amazon;
			break;
		case App.XDA_DEVELOPERS:
			iconId = R.drawable.btn_xda;
			break;
		case App.OTHER:
			iconId = R.drawable.btn_link_other;
			break;
		default:
			iconId = R.drawable.icon_not_sure;
			break;
		}
		
		return mContext.getResources().getDrawable(iconId);
	}
	
	
}
