package it.tiwiz.shareitapp;

import android.graphics.drawable.Drawable;

public class Report {
	
	private String type = "";
	private String description = "";
	private Drawable icon;
	
	public Report(String type, String description, Drawable icon){
		this.type = type;
		this.description = description;
		this.icon = icon;
	}

	public Report() {}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Drawable getIcon() {
		return icon;
	}

	public void setIcon(Drawable icon) {
		this.icon = icon;
	}
}
