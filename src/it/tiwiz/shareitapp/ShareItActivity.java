package it.tiwiz.shareitapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.devspark.appmsg.AppMsg;
import com.fortysevendeg.android.swipelistview.SwipeListView;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import com.bugsense.trace.BugSenseHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ShareItActivity extends SherlockActivity implements OnClickListener,OnItemClickListener{
	
	private static List<App> installedApps = null;
	public static SwipeListView installedAppsList;
	public static Context mContext;
	public static ShareItActivity mActivity;
	public static AppAdapter adapter = null;
	public static ProgressBar loadingBar;
	public static List<App> selectedApps = null;
	public static View btnCancelSelection;
	public static RelativeLayout btnBackground;
	public static RelativeLayout pBarBackground;
	private static MenuItem menuItem;
	private static PackageReceiver pr;
    public static int buttonHeight = 200;
	private Dialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        BugSenseHandler.initAndStartSession(ShareItActivity.this, "df79aa8f");
		setContentView(R.layout.activity_share_it);
		
		ActionBar ab = getSupportActionBar();
		ab.setCustomView(R.layout.custom_title);
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mContext = this;
		mActivity = this;
		new LoadApps().execute();
		AppMsg.makeText(this, R.string.loading_app_list, AppMsg.STYLE_INFO).show();
		
		btnCancelSelection = findViewById(R.id.btnCancelSelection);
		btnCancelSelection.setOnClickListener(this);

		btnBackground = (RelativeLayout) findViewById(R.id.btnCancelAllLayout);
        final Object[] params = makeFakeActionBar(this);
        if (params != null)
        {
            final View view = btnBackground;
            final Drawable background = (Drawable)params[0];
            final int height = (Integer)params[1];

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams)view.getLayoutParams();
            lp.height = height;
            view.setLayoutParams(lp);
            if (Build.VERSION.SDK_INT < 16) //noinspection deprecation
                 view.setBackgroundDrawable(background);
            else view.setBackground (background);
        }


	}
	
	protected void onResume(){
		
		//register receiver
		pr = new PackageReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
		intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		intentFilter.addDataScheme("package");
		registerReceiver(pr,intentFilter);
		
		super.onResume();
	}
	
	protected void onPause(){
		
		if(pr!=null) {
            unregisterReceiver(pr);
        }

		super.onPause();
	}

    @Override
    protected void onStop(){
        BugSenseHandler.closeSession(ShareItActivity.this);
        super.onStop();
    }

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.share_it, menu);
		menuItem = menu.findItem(R.id.btnRefresh);
		
		int apiLevel = Build.VERSION.SDK_INT;
		int versionCode = Build.VERSION_CODES.HONEYCOMB;
		
		if(apiLevel >= versionCode){
			
			final SearchView searchView = (SearchView) menu.findItem(R.id.btnSearch).getActionView();
			
			searchView.setOnQueryTextListener(new OnQueryTextListener(){
	
				@Override
				public boolean onQueryTextChange(String SearchedString) {

                    if(null == adapter) return false;

                    installedAppsList.closeAll();
                    adapter.notifyDataSetChanged();
                    adapter.getFilter().filter(SearchedString);

					return false;
				}
	
				@Override
				public boolean onQueryTextSubmit(String SearchedString) {
                    if(null == adapter) return false;

                    installedAppsList.closeAll();
                    adapter.getFilter().filter(SearchedString);

					return false;
				}});
		}else{
			(menu.findItem(R.id.btnSearch)).setVisible(false);
		}
		
		//RIMUOVERE QUANDO SI FA L'EXPORT
		(menu.findItem(R.id.btnExport)).setVisible(false);
		return true;
	}
	
	 @Override
     public boolean onOptionsItemSelected(MenuItem item) {
		 
		 switch(item.getItemId()){
		 case R.id.btnRefresh:
			 AppMsg.makeText(this, R.string.refresh_app_message, AppMsg.STYLE_INFO).show();
			 menuItem = item;
			 menuItem.setActionView(R.layout.actionbar_progress);
			 menuItem.expandActionView();
			 new LoadApps().execute();
			 break;
		 case R.id.btnShareMulti:
             if(null != selectedApps){ //fix for clicks before apps get loaded
                 if(selectedApps.size() > 0)
                     Intents.shareMoreLinks(mContext, selectedApps);
                 else
                     Intents.shareMoreLinks(mContext, installedApps);
             }
             break;
         case R.id.btnMultiAPK:
             if(null != selectedApps){

                 if(selectedApps.size() > 0)
                     Intents.shareMoreAPKs(mContext,selectedApps);
                 else
                     Intents.shareMoreAPKs(mContext,installedApps);
             }
             break;
		 case R.id.btnExport:
			 showExportMenu();
			 break;
		 case R.id.btnAbout:
			 Intents.startAbout(mContext);
			 break;
		 }
		 
		 return super.onOptionsItemSelected(item);
		 
	 }
	 
	 public static void broadcastRefresh(){
		 if(mActivity!=null)
			 mActivity.new LoadApps().execute();
	 }
	 public void showExportMenu(){
		 
		 ContextThemeWrapper context = new ContextThemeWrapper(this, R.style.AppBaseTheme);
		 dialog = new Dialog(context, R.style.Holo_Theme_Dialog_Light);
		 dialog.setTitle(R.string.report_export_title);
		 dialog.setCancelable(true);
		 dialog.setContentView(R.layout.export_layout);
		 
		 List<Report> reports = ReportWriter.getReportVoices(mContext);
		 ListView lv = (ListView) dialog.findViewById(R.id.listReports);
		 ReportAdapter reportAdapter = new ReportAdapter(this, reports);
		 lv.setAdapter(reportAdapter);
		 reportAdapter.notifyDataSetChanged();
		 lv.setOnItemClickListener(this);
	
		 dialog.show();
	 }
	 
	 public void askForTutorial(){
		 
		 ContextThemeWrapper context = new ContextThemeWrapper(this, R.style.AppBaseTheme);
		 final Dialog firstTime = new Dialog(context,R.style.Holo_Theme_Dialog_Light);
		 firstTime.setCancelable(false);
		 firstTime.setTitle(R.string.first_time_title);
		 firstTime.setContentView(R.layout.first_time_dialog);
		 
		 Button btnFirstTimeYes = (Button) firstTime.findViewById(R.id.btnFirstTimeYes);
		 Button btnFirstTimeNo = (Button) firstTime.findViewById(R.id.btnFirstTimeNo);
		 
		 btnFirstTimeYes.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				firstTime.dismiss();
				Intents.startTutorial(mContext);
			}});
		 
		 btnFirstTimeNo.setOnClickListener(new OnClickListener(){
				@Override
			public void onClick(View arg0) {
				firstTime.dismiss();
			}});
		 
		 firstTime.show();
	 }
	 
	public class LoadApps extends AsyncTask<Void,Void,Void>{
		
		public LoadApps(){
			//loadingBar = (ProgressBar) findViewById(R.id.pBarLoading);
			pBarBackground = (RelativeLayout) findViewById(R.id.pBarBackground);
			installedAppsList = (SwipeListView) findViewById(R.id.appList);

    	}
		
		@Override
		protected Void doInBackground(Void... arg0) {		
			installedApps = PMFilter.getInstalledApps(mContext);
			Collections.sort(installedApps);
			selectedApps = new ArrayList<App>();
			if(adapter != null){
				runOnUiThread(new Runnable() {
				     public void run() {
				    	 adapter.clear();
				    	 adapter.notifyDataSetChanged();
				    	 adapter = new AppAdapter(mContext,installedApps);
				    	 installedAppsList.setAdapter(adapter);
				    	 adapter.notifyDataSetChanged();
				    }
				});
			}else{
				adapter = new AppAdapter(mContext,installedApps);
				runOnUiThread(new Runnable() {
				     public void run() {
				    	 installedAppsList.setAdapter(adapter);
				    	 adapter.notifyDataSetChanged();
				    }
				});
			}
			installedAppsList.setSwipeCloseAllItemsWhenMoveList(true);
			
			return null;
		}
		
		@ Override
	    protected void onPreExecute ( ) {
	       //loadingBar.setVisibility(View.VISIBLE);
			if(menuItem != null){
				menuItem.setActionView(R.layout.actionbar_progress);
				menuItem.expandActionView();
			}
			pBarBackground.setVisibility(View.VISIBLE);
			Animation ani = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
			pBarBackground.startAnimation(ani);
	    }
		
		@ Override
	    protected void onPostExecute ( Void result ) {
	        //loadingBar.setVisibility(View.GONE);
	        Animation ani = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
	        pBarBackground.startAnimation(ani);
	        pBarBackground.setVisibility(View.GONE);
	        if(menuItem != null){
		        menuItem.collapseActionView();
		        menuItem.setActionView(null);
	        }
	        
	        final boolean runTutorial = StoredData.firstTimeAppRun(mContext);
	        runOnUiThread(new Runnable() {
			     public void run() {
			    	 if(runTutorial){
			    		 askForTutorial();
			    	 }
			    }
			});
	    }
	}


	@Override
	public void onClick(View clickedView) {
		
		switch(clickedView.getId()){
		case R.id.btnCancelSelection:
			adapter.uncheckAll();
			UIManager.hideCancelSelectionButton();
			break;
		}
		
	}


	@Override
	public void onItemClick(AdapterView<?> parentView, View childView, int position, long id) {
		
		List<App> parameter;
		
		if(selectedApps.size() > 0)
			parameter = selectedApps;
		else
			parameter = installedApps;
		
		switch(position){
		case ReportWriter.PLAIN_TEXT: //PLAIN TEXT
			ReportWriter.exportASCIIFormat(mActivity, mContext, parameter, ReportWriter.PLAIN_TEXT);
			break;
		case ReportWriter.CSV: //CSV
			ReportWriter.exportASCIIFormat(mActivity, mContext, parameter, ReportWriter.CSV);
			break;
		case ReportWriter.XML: //XML
			ReportWriter.exportASCIIFormat(mActivity, mContext, parameter, ReportWriter.XML);
			break;
		case ReportWriter.HTML: //HTML
			ReportWriter.exportASCIIFormat(mActivity, mContext, parameter, ReportWriter.HTML);
			break;
		case ReportWriter.PDF: //PDF
			break;
		}
		
		dialog.dismiss();
		
	}

    private static Object[] makeFakeActionBar(final ContextThemeWrapper contextThemeWrapper)
    {
        TypedArray actionBarStyle = null;
        try
        {
            final boolean newApi = Build.VERSION.SDK_INT >= 14;
            final Resources.Theme theme = contextThemeWrapper.getTheme();
            final TypedValue attribute = new TypedValue();
            theme.resolveAttribute(newApi ? android.R.attr.actionBarStyle : R.attr.actionBarStyle, attribute, true);
            actionBarStyle = theme.obtainStyledAttributes(attribute.resourceId, new int[] { newApi ? android.R.attr.background : R.attr.background });
            final Drawable actionBarBackground = actionBarStyle.getDrawable(0);
            actionBarStyle.recycle();
            actionBarStyle = null;
            final Resources res = contextThemeWrapper.getResources();
            theme.resolveAttribute(newApi ? android.R.attr.windowContentOverlay : R.attr.windowContentOverlay, attribute, true);
            final Drawable actionBarShadow = res.getDrawable(attribute.resourceId);
            final int actionBarShadowHeight = actionBarShadow.getIntrinsicHeight();
            theme.resolveAttribute(newApi ? android.R.attr.actionBarSize : R.attr.actionBarSize, attribute, true);
            final int actionBarHeight = res.getDimensionPixelSize(attribute.resourceId);
            final int height = actionBarHeight + actionBarShadowHeight;
            buttonHeight = actionBarHeight;

            final LayerDrawable layers = new LayerDrawable(new Drawable[] { actionBarBackground, actionBarShadow });
            layers.setLayerInset(0, 0, 0, 0, actionBarShadowHeight); // actionBarBackground
            layers.setLayerInset(1, 0, actionBarHeight, 0, 0);       // actionBarShadow
            final RotateDrawable rotated = new RotateDrawable(layers, 180, 180);

            return new Object[] { rotated, height };
        }
        catch (Exception ignore)
        {
            return null;
        }
        finally
        {
            if (actionBarStyle != null) actionBarStyle.recycle();
        }
    }

}
