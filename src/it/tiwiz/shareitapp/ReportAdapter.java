package it.tiwiz.shareitapp;


import java.util.List;

import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ReportAdapter extends BaseAdapter{
	
	private Context mContext;
	private List<Report> types;

	public ReportAdapter(Context context, List<Report> types) {

		this.mContext = context;
		this.types = types;
	}
	
	@Override
    public View getView(int position, View contentView, ViewGroup parent){
		Holder reports = null;
		
		if(contentView == null){
			LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView = li.inflate(R.layout.report_element, parent, false);
			
			reports = new Holder();
			reports.icon = (ImageView) contentView.findViewById(R.id.imgReportIcon);
			reports.type = (TextView) contentView.findViewById(R.id.txtReportType);
			reports.description = (TextView) contentView.findViewById(R.id.txtReportDescription);
			
			contentView.setTag(reports);
		}else
			reports = (Holder) contentView.getTag();
		
		if(position >= types.size()) return null;
		
		Report current = types.get(position);
		
		reports.icon.setImageDrawable(current.getIcon());
		reports.type.setText(current.getType());
		reports.description.setText(current.getDescription());
	
		return contentView;
	}
	
	static class Holder{
		ImageView icon;
		TextView type, description;
	}

	@Override
	public int getCount() {
		return types.size();
	}

	@Override
	public Object getItem(int arg0) {
		return types.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
}
