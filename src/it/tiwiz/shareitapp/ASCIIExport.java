package it.tiwiz.shareitapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ASCIIExport implements Runnable{
	
	private List<App> apps;
	private int format;
	
	public ASCIIExport(List<App> apps, int format){
		this.apps = apps;
		this.format = format;
	}
	
	@Override
	public void run() {
		String filename = "";
		switch(format){
		case ReportWriter.PLAIN_TEXT:
			filename = chooseName("txt");
			break;
		case ReportWriter.CSV:
			filename = chooseName("csv");
			break;
		case ReportWriter.XML:
			filename = chooseName("xml");
			break;
		case ReportWriter.HTML:
			filename = chooseName("html");
			break;
		}
	}
	
	private String chooseName(String extension){
		
		StringBuilder filename = new StringBuilder();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String currentDateAndTime = sdf.format(new Date());
		
		filename.append("ShareItApp_export_");
		filename.append(currentDateAndTime + ".");
		filename.append(extension);
		
		return filename.toString();
	}

}
