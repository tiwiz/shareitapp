package it.tiwiz.shareitapp;

import java.util.List;
import java.util.Vector;
import com.viewpagerindicator.PageIndicator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

public class TutorialActivity extends FragmentActivity {
	
	ViewPager mPager;
	PageIndicator mIndicator;
	List<Fragment> fragments = new Vector<Fragment>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tutorial_main_page);
		
		fragments.add(Fragment.instantiate(this, TutorialFirstPage.class.getName()));
		fragments.add(Fragment.instantiate(this, TutorialSecondPage.class.getName()));
		fragments.add(Fragment.instantiate(this, TutorialThirdPage.class.getName()));
		
		FragmentManager fm = getSupportFragmentManager();
		
		TutorialAdapter adapter = new TutorialAdapter(fm,fragments);
		
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(adapter);
		
		mIndicator = (PageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);
	}
	
	@Override
	public void onBackPressed() {
	    finish();
	    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
	}
}
