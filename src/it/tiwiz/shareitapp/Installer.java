package it.tiwiz.shareitapp;

public class Installer {
	
	private int type;
	private String details;
	
	public Installer(){
		this(-1,"");
	}
	
	public Installer(int type, String details){
		this.type = type;
		this.details = details;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}
