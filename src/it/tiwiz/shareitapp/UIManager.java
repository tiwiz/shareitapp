package it.tiwiz.shareitapp;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class UIManager {
	
	private static boolean cancelSelectionButtonShowing = false;
    private static int paddingBottom = -1;

	public static void showCancelSelectionButton(){

  		cancelSelectionButtonShowing = true;
		ShareItActivity.btnBackground.setVisibility(View.VISIBLE);
		Animation ani = AnimationUtils.loadAnimation(ShareItActivity.mContext, R.anim.slide_up);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                int left = ShareItActivity.installedAppsList.getPaddingLeft();
                int right = ShareItActivity.installedAppsList.getPaddingRight();
                int top = ShareItActivity.installedAppsList.getPaddingTop();

                paddingBottom =  ShareItActivity.installedAppsList.getPaddingBottom();

                ShareItActivity.installedAppsList.setPadding(left,top,right,ShareItActivity.buttonHeight);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
		ShareItActivity.btnBackground.startAnimation(ani);

	}
	
	public static void hideCancelSelectionButton(){



		cancelSelectionButtonShowing = false;
		Animation ani = AnimationUtils.loadAnimation(ShareItActivity.mContext, R.anim.slide_down);

        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                int left = ShareItActivity.installedAppsList.getPaddingLeft();
                int right = ShareItActivity.installedAppsList.getPaddingRight();
                int top = ShareItActivity.installedAppsList.getPaddingTop();

                if(paddingBottom < 0) paddingBottom = 0;

                ShareItActivity.installedAppsList.setPadding(left,top,right,paddingBottom);
            }
            @Override
            public void onAnimationEnd(Animation animation) {}
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
		ShareItActivity.btnBackground.startAnimation(ani);
		ShareItActivity.btnBackground.setVisibility(View.GONE);

	}
	
	public static boolean isCancelSelectionButtonShowing(){
		return cancelSelectionButtonShowing;
	}
}