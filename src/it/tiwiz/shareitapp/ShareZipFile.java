package it.tiwiz.shareitapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User   : PC-Roby
 * Date   : 28/04/13
 * Time   : 10.22
 * Project: ShareItApp
 * Package: it.tiwiz.shareitapp
 */
public class ShareZipFile implements Runnable{

    private static String folderPath;
    private static boolean folderAvailable = false;

    private Context mContext;
    private ArrayList<App> apps;

    private static final String SHARE_DIR = "ShareItApp";
    private static final String DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";
    private static final int BUFFER = 2048;

    public ShareZipFile(Context mContext, List<App> apps){

        this.mContext = mContext;
        this.apps = new ArrayList<App>(apps);

    }

    private String createZipName(){

        StringBuilder name = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

        String device = "";

        if(Build.DEVICE.length() > 0)
            device = Build.DEVICE;
        else
            device = Build.MODEL;

        int appListSize = apps.size();

        String apkSuffix = mContext.getResources().getQuantityString(R.plurals.plural_apk,appListSize);

        name.append(folderPath + "/");
        name.append(device + "_");
        name.append(appListSize + apkSuffix + "_");
        name.append(sdf.format(new Date()));
        name.append(".zip");

        return name.toString();
    }

    private void checkStorage(){

        String mediaState = Environment.getExternalStorageState();

        if(Environment.MEDIA_MOUNTED.equals(mediaState)){
            //storage is both readable and writable
            File root = Environment.getExternalStorageDirectory();
            String rootPath = root.getPath();

            File shareDir = new File(rootPath + "/" + SHARE_DIR);
            if(shareDir.exists() && shareDir.isDirectory()){
                //Directory already exists, we use that one
                folderAvailable = true;
                folderPath = shareDir.getPath();
            }else{
                //Directory does not exist yet, we try and create a new one
                folderAvailable = shareDir.mkdir();
                folderPath = (folderAvailable == true)?shareDir.getPath():"";
            }
        }else{
            folderPath = "";
            folderAvailable = false;
        }

    }

    @Override
    public void run() {

        checkStorage();

        //checks if folder is available
        if(folderAvailable){
            //Gets Application's PID
            int myPid = android.os.Process.myPid();
            String zipName = createZipName(); //creates filename



            //creates notification
            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            //creates builder for notification
            NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(mContext);
            //Adds notification title
            nBuilder.setContentTitle(mContext.getResources().getString(R.string.app_name)); //Title of notification
            //and icon
            nBuilder.setSmallIcon(R.drawable.ic_stat_notification_icon);

            Bitmap largeIcon = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.ic_stat_notification_icon);
            nBuilder.setLargeIcon(largeIcon);

            nBuilder.setAutoCancel(true);

            //creates ZipFile
            try{
                //Declare used streams and variables
                BufferedInputStream bis = null;
                FileOutputStream fos = new FileOutputStream(zipName);
                ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos));
                byte data[] = new byte[BUFFER];
                int numberOfApps = apps.size();

                for(int i = 0; i < numberOfApps; i++){
                    String packagePath = apps.get(i).getPackagePath();
                    FileInputStream fis = new FileInputStream(packagePath);
                    bis = new BufferedInputStream(fis,BUFFER);
                    //Adds entry name
                    ZipEntry entry = new ZipEntry(packagePath.substring(packagePath.lastIndexOf("/") + 1));
                    zos.putNextEntry(entry); //Puts entry in zip file
                    int count;
                    while((count = bis.read(data,0,BUFFER)) != -1) zos.write(data,0,count); //adds file to the zip
                    bis.close();

                    String message = mContext.getString(R.string.notification_working,(i+1),numberOfApps); //personalized message for each app
                    String subText = mContext.getString(R.string.notification_in_progress, apps.get(i).getAppName());


                    nBuilder.setContentText(message);
                    nBuilder.setProgress(numberOfApps, (i+1), false);
                    nBuilder.setSubText(subText);
                    notificationManager.notify(myPid,nBuilder.build()); //notification while working
                }

                //closes Zip
                zos.close();

                //warns user that everything goes as expected
                String message = mContext.getString(R.string.notification_finished);
                String trimmedZipName = zipName.substring(zipName.lastIndexOf("/") + 1);
                nBuilder.setContentText(message);
                nBuilder.setTicker(mContext.getString(R.string.notification_finished_ticker,trimmedZipName));

                //Creates share intent for the zip
                PendingIntent shareZip;

                //based on API version, it will create a notification pendingIntent or another
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                    shareZip = Intents.allAndroidZipIntent(mContext,zipName,myPid, false);
                    nBuilder.addAction(R.drawable.btn_notification_share,mContext.getString(R.string.share_text),shareZip);
                    nBuilder.setSubText(mContext.getString(R.string.notification_finished_subtext_empty));
                }else{
                    shareZip = Intents.allAndroidZipIntent(mContext,zipName,myPid, true);
                    nBuilder.setContentIntent(shareZip);
                    nBuilder.setSubText(mContext.getString(R.string.notification_finished_subtext));
                }

                notificationManager.notify(myPid,nBuilder.build());

            }catch(Exception e){

                //delete created zip if something fails
                boolean deleted = true;
                File zipToDelete = new File(zipName);
                if(zipToDelete.exists()) deleted = zipToDelete.delete();

                String message = mContext.getString(R.string.notification_error);

                nBuilder.setContentText(message);
                nBuilder.setSmallIcon(R.drawable.ic_stat_notification_icon_error);
                nBuilder.setSubText(mContext.getString(R.string.notification_error_subtext));

                largeIcon = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.ic_stat_notification_icon_error);
                nBuilder.setLargeIcon(largeIcon);

                String trimmedZipName = zipName.substring(zipName.lastIndexOf("/") + 1);
                PendingIntent openActivity = Intents.createErrorReportIntent(mContext,e.getMessage(),deleted, trimmedZipName);

                nBuilder.setContentIntent(openActivity);

                notificationManager.notify(myPid,nBuilder.build());
            }
        }
    }
}
