package it.tiwiz.shareitapp;

import java.util.ArrayList;
import java.util.List;

import com.devspark.appmsg.AppMsg;

import android.content.Context;
import android.os.Environment;

public class ReportWriter {
	
	public static final int PLAIN_TEXT = 0;
	public static final int CSV = 1;
	public static final int XML = 2;
	public static final int HTML = 3;
	public static final int PDF = 4;
	
	private static boolean isExternalStorageWritable(){
		
		String currentState = Environment.getExternalStorageState();
		
		if(Environment.MEDIA_MOUNTED.equals(currentState))
			return true;
		
		return false;
	}
	
	public static void exportASCIIFormat(ShareItActivity mActivity, Context mContext, List<App> apps, int format){
		
		if(format < PLAIN_TEXT || format > HTML) return;
		if(isExternalStorageWritable()){
			new ASCIIExport(apps,format).run();
		}else
			AppMsg.makeText(mActivity, R.string.memory_not_available, AppMsg.STYLE_ALERT).show();
		
	}
	
	public static List<Report> getReportVoices(Context mContext){
		
		List<Report> reports = new ArrayList<Report>();
		Report tmpReport;
		
		//Plain text report
		tmpReport = new Report();
		tmpReport.setType(mContext.getString(R.string.report_plain_text));
		tmpReport.setDescription(mContext.getString(R.string.report_plain_text_description));
		tmpReport.setIcon(mContext.getResources().getDrawable(R.drawable.report_plain_text));
		reports.add(tmpReport);
		//CSV report
		tmpReport = new Report();
		tmpReport.setType(mContext.getString(R.string.report_csv));
		tmpReport.setDescription(mContext.getString(R.string.report_csv_description));
		tmpReport.setIcon(mContext.getResources().getDrawable(R.drawable.report_csv));
		reports.add(tmpReport);
		//XML report
		tmpReport = new Report();
		tmpReport.setType(mContext.getString(R.string.report_xml));
		tmpReport.setDescription(mContext.getString(R.string.report_xml_description));
		tmpReport.setIcon(mContext.getResources().getDrawable(R.drawable.report_xml));
		reports.add(tmpReport);
		//HTML report
		tmpReport = new Report();
		tmpReport.setType(mContext.getString(R.string.report_html));
		tmpReport.setDescription(mContext.getString(R.string.report_html_description));
		tmpReport.setIcon(mContext.getResources().getDrawable(R.drawable.report_html));
		reports.add(tmpReport);
		//PDF report
		tmpReport = new Report();
		tmpReport.setType(mContext.getString(R.string.report_pdf));
		tmpReport.setDescription(mContext.getString(R.string.report_pdf_description));
		tmpReport.setIcon(mContext.getResources().getDrawable(R.drawable.report_pdf));
		reports.add(tmpReport);
		
		return reports;
	}
}
