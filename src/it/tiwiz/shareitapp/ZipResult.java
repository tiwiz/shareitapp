package it.tiwiz.shareitapp;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.devspark.appmsg.AppMsg;
import org.holoeverywhere.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User   : PC-Roby
 * Date   : 28/04/13
 * Time   : 12.28
 * Project: ShareItApp
 * Package: it.tiwiz.shareitapp
 */
public class ZipResult extends SherlockActivity{

    TextView errorReport;
    static boolean developerAware = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zip_result);

        errorReport = (TextView) findViewById(R.id.textReportResult);

        Bundle extras = getIntent().getExtras();

        String message = extras.getString(Intents.ZIP_ERROR_MESSAGE);
        boolean deleted = extras.getBoolean(Intents.ZIP_ERROR_DELETE, false);
        String zipName = extras.getString(Intents.ZIP_ERROR_NAME);

        manageError(message,deleted,zipName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.error_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
        case R.id.btnSendErrorReport:
            String message = errorReport.getText().toString();
            if(developerAware == true)
                AppMsg.makeText(this,R.string.developer_aware,AppMsg.STYLE_INFO).show();
            else if(null != message)
                    Intents.sendZipErrorResult(this,message);
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void manageError(String message, boolean deleted, String zipName){

        StringBuilder error = new StringBuilder();
        String customMessage = "";
        String customZipMessage = "";

        if(null == message) customMessage = getString(R.string.zip_error_message_null);
        else{
            if(message.contains("EACCES (Permission denied)")){ //App cannot be included in a Zip File
                String app = message.substring(0,message.lastIndexOf("apk:") + 3);
                customMessage = getString(R.string.zip_error_eaccess,app);
                developerAware = true;
            }else
                customMessage = message;
        }

        if(null == zipName) customZipMessage = getString(R.string.zip_error_zipname_null);
        else{
            if(deleted == true) customZipMessage = getString(R.string.zip_deleted,zipName);
            else customZipMessage = getString(R.string.zip_not_deleted,zipName);
        }

        error.append(customMessage + "\n\n");
        error.append(customZipMessage);

        errorReport.setText(error.toString());
    }
}