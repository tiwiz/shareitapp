package it.tiwiz.shareitapp;

import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;

/**
 * Created with IntelliJ IDEA.
 * User   : PC-Roby
 * Date   : 28/04/13
 * Time   : 21.17
 * Project: ShareItApp
 * Package: it.tiwiz.shareitapp
 */
public class InvisibleActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Transparent);
        super.onCreate(savedInstanceState);

        String zipPath = getIntent().getExtras().getString(Intents.ZIP_PATH);
        int requestId = getIntent().getExtras().getInt(Intents.REQUEST_PID);

        if(null != zipPath){
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(requestId);
            startActivity(Intents.createZipIntent(zipPath));
        }
    }
}