package it.tiwiz.shareitapp;

import java.util.List;

import android.app.Notification;
import android.app.PendingIntent;
import android.util.Log;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.widget.*;
import org.holoeverywhere.widget.AdapterView.OnItemSelectedListener;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;

public class Intents {

    public final static String ZIP_ERROR_MESSAGE = "zipErrorMessage";
    public final static String ZIP_ERROR_DELETE = "zipErrorDelete";
    public final static String ZIP_ERROR_NAME = "zipErrorName";
    public final static String ZIP_PATH = "zipPath";
    public final static String REQUEST_PID = "requestPid";
	
	
	public static void startPlayStore(Context mContext, App app){
		//starts Play Store for given app
		int type = app.getAlternativeSource();
		
		String strUri;
		if(type != App.NOT_SET){
			switch(type){
			case App.PLAY_STORE:
				strUri = C.PLAY_STORE_ANDROID_HEADER + app.getPackageName();
				break;
			case App.AMAZON_APP_SHOP:
				strUri = C.AMAZON_HEADER;
				break;
			case App.XDA_DEVELOPERS:
				strUri = C.XDA_HEADER + app.getAlternativeDetails();
				break;
			case App.OTHER:
			default:
				strUri = app.getAlternativeDetails();
				if (!strUri.startsWith("http://") && !strUri.startsWith("https://"))
					strUri = "http://" + strUri;
				break;
			}
		}else{
			strUri = C.PLAY_STORE_ANDROID_HEADER + app.getPackageName();
		}
		
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(strUri));
		mContext.startActivity(intent);
	}
	
	public static void startAppOptions(final Context mContext, final App app){
		
		ContextThemeWrapper context = new ContextThemeWrapper(mContext, R.style.AppBaseTheme);
		final Dialog dialog = new Dialog(context, R.style.Holo_Theme_Dialog_Light);
		dialog.setTitle(R.string.app_options_title);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.app_options);
		
		final Spinner spinner = (Spinner) dialog.findViewById(R.id.app_source_spinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext, R.array.apps_sources, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		
		final EditText txtDetails = (EditText) dialog.findViewById(R.id.app_source_detail);
		txtDetails.setError(null);
		
		int alternativeMarket = app.getAlternativeSource();
		if(alternativeMarket != App.NOT_SET){
			spinner.setSelection(alternativeMarket);
			if(alternativeMarket >= App.XDA_DEVELOPERS){
				txtDetails.setVisibility(View.VISIBLE);
				txtDetails.setText(app.getAlternativeDetails());
			}
		}
		
		spinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				
				switch(position){
				case App.PLAY_STORE: //Play Store
					txtDetails.setVisibility(View.INVISIBLE);
					break;
				case App.AMAZON_APP_SHOP: //Amazon App-Shop
					txtDetails.setVisibility(View.INVISIBLE);
					break;
				case App.XDA_DEVELOPERS: //XDA
					txtDetails.setHint(R.string.app_options_detail_xda);
					txtDetails.setVisibility(View.VISIBLE);
					break;
				case App.OTHER: //Other
					txtDetails.setHint(R.string.app_options_detail_other);
					txtDetails.setVisibility(View.VISIBLE);
					break;
				default:
					break;
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}});
		
		Button btnCancel = (Button) dialog.findViewById(R.id.btnAppOptionsCancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});
		
		Button btnSave = (Button) dialog.findViewById(R.id.btnAppOptionsSave);
		btnSave.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				int selection = spinner.getSelectedItemPosition();
				switch(selection){
				case App.PLAY_STORE:
					StoredData.saveInstaller(mContext, app.getPackageName(), App.PLAY_STORE);
					dialog.dismiss();
					break;
				case App.AMAZON_APP_SHOP:
					StoredData.saveInstaller(mContext, app.getPackageName(), App.AMAZON_APP_SHOP);
					dialog.dismiss();
					break;
				case App.XDA_DEVELOPERS:
					String id = txtDetails.getEditableText().toString();
					if(id.contentEquals("") || id.length() == 0)
						txtDetails.setError(mContext.getString(R.string.app_options_error));
					else{
						txtDetails.setError(null);
						StoredData.saveInstaller(mContext, app.getPackageName(), App.XDA_DEVELOPERS,id);
						dialog.dismiss();
					}
					break;
				case App.OTHER:
					String url = txtDetails.getEditableText().toString();
					if(url.contentEquals("") || url.length() == 0)
						txtDetails.setError(mContext.getString(R.string.app_options_error));
					else{
						txtDetails.setError(null);
						StoredData.saveInstaller(mContext, app.getPackageName(), App.OTHER,url);
						dialog.dismiss();
					}
					break;
				default:
					break;
				}
				
				
				
			}});
		dialog.show();
	}
	
	public static void startAbout(final Context mContext){
		
		ContextThemeWrapper context = new ContextThemeWrapper(mContext, R.style.AppBaseTheme);
		final Dialog dialog = new Dialog(context, R.style.Holo_Theme_Dialog_Light);
		dialog.setTitle(R.string.btn_about);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.about_dialog);
				
		Button btnTutorial = (Button) dialog.findViewById(R.id.btnAboutRunTutorial);
		btnTutorial.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				//start tutorial
				dialog.dismiss();
				startTutorial(mContext);
			}});
		
		Button btnRate = (Button) dialog.findViewById(R.id.btnAboutRate);
		btnRate.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				String strUri = C.PLAY_STORE_ANDROID_HEADER + "it.tiwiz.shareitapp";
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(strUri));
				mContext.startActivity(intent);
				ShareItActivity.mActivity.overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
			}
			
		});
		
		dialog.show();
	}
	
	public static void startTutorial(Context mContext){
		Intent tutorial = new Intent(mContext, TutorialActivity.class);
		mContext.startActivity(tutorial);
		ShareItActivity.mActivity.overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
	}
	
	public static void uninstallApp(Context mContext, String packageName){
		int apiLevel = Build.VERSION.SDK_INT;
		
		String strUri = "package:" + packageName;
		Uri uri = Uri.parse(strUri);
		Intent uninstall;
		
		if(apiLevel >= 14) //Ice Cream Sandwich
			uninstall = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, uri);
		else
			uninstall = new Intent(Intent.ACTION_DELETE, uri);
		
		mContext.startActivity(uninstall);
	}
	
	
	public static void openAppInfo(Context mContext, String packageName){
		int apiLevel = Build.VERSION.SDK_INT;
		
		if(apiLevel > 8){ //Gingerbread and later
			Intent i = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		    i.addCategory(Intent.CATEGORY_DEFAULT);
		    i.setData(Uri.parse("package:" + packageName));
		    mContext.startActivity(i);
		}else{
			Intent i = new Intent();
		    i.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
		    i.setAction(Intent.ACTION_VIEW);
		    i.putExtra("pkg", packageName);
		    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    try{
		    	mContext.startActivity(i);
		    }catch(Exception e){
		    	//do - nothing
		    }
		}
	}
	
	
	private static String createLink(Context mContext, App app){
		
		int alternativeMarket = app.getAlternativeSource();
		String link;
		final String possibleLink = mContext.getResources().getString(R.string.txt_possible_link);
		
		if(alternativeMarket != App.NOT_SET){
			switch(alternativeMarket){
			case App.PLAY_STORE:
				link = app.getAppName() + " (" + C.PLAY_STORE_WEB_HEADER + app.getPackageName() + ")";
				break;
			case App.AMAZON_APP_SHOP:
				link = app.getAppName() + " - " + mContext.getResources().getString(R.string.get_from_amazon);
				break;
			case App.XDA_DEVELOPERS:
				link = app.getAppName() + " - " + C.XDA_HEADER + app.getAlternativeDetails();
				break;
			case App.OTHER:
			default:
				link = app.getAppName() + " - " + app.getAlternativeDetails();
				break;
			}
		}else{
			if(app.isInstalledFromPlayStore())
				link = app.getAppName() + " (" + C.PLAY_STORE_WEB_HEADER + app.getPackageName() + ")";
			else
				link = app.getAppName() + " (" + C.PLAY_STORE_WEB_HEADER + app.getPackageName() + " " + possibleLink +")";
		}
		
		return link + " - ";
	}
	
	private static String createWebLink(Context mContext, App app){
		
		int alternativeMarket = app.getAlternativeSource();
		StringBuilder builder = new StringBuilder();
		String link, label = app.getAppName();
		final String possibleLink = mContext.getResources().getString(R.string.txt_possible_link);
		
		if(alternativeMarket != App.NOT_SET){
			switch(alternativeMarket){
			case App.PLAY_STORE:
				link = C.PLAY_STORE_WEB_HEADER + app.getPackageName();
				break;
			case App.AMAZON_APP_SHOP:
				link = C.AMAZON_HEADER;
				break;
			case App.XDA_DEVELOPERS:
				link = C.XDA_HEADER + app.getAlternativeDetails();
				label = app.getAppName() + " - " + mContext.getResources().getString(R.string.get_from_amazon);
				break;
			case App.OTHER:
			default:
				link = app.getAlternativeDetails();
				break;
			}
		}else{
			link = C.PLAY_STORE_WEB_HEADER + app.getPackageName();
			if(!app.isInstalledFromPlayStore())
				label = app.getAppName() + " - " + possibleLink;
		}
		builder.append("<a href=\"");
		builder.append(link);
		builder.append("\">");
		builder.append(label);
		builder.append("</a>");
		return builder.toString();
	}
	public static void shareLink(Context mContext,App app){
		
		
		final String sharedWith = mContext.getResources().getString(R.string.share_it_app_promo_text);
		final String shareTitle = mContext.getResources().getString(R.string.share_with);
		
		
		StringBuilder builder = new StringBuilder();
		
		String link = createLink(mContext,app);
		builder.append(link);
		
		builder.append(sharedWith);
		
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, sharedWith); 
		shareIntent.putExtra(Intent.EXTRA_TEXT, builder.toString());
		
		mContext.startActivity(Intent.createChooser(shareIntent, shareTitle));
		
	}
	
	public static void shareAPK(Context mContext,App app){
		
		final String sharedWith = mContext.getResources().getString(R.string.share_it_app_promo_text);
		final String shareTitle = mContext.getResources().getString(R.string.share_with);
		final String headerString = mContext.getResources().getString(R.string.txt_share_apk_header);
		final String footerString = mContext.getResources().getString(R.string.txt_share_apk_footer);
		final String getTheAppString = mContext.getResources().getString(R.string.txt_get_the_app);

		StringBuilder builder = new StringBuilder();
		builder.append(headerString);
		builder.append("<br>");
		builder.append(createWebLink(mContext,app));
		builder.append("<br>");
		builder.append(footerString);
		builder.append("<br>");
		builder.append("<a href=\"https://play.google.com/store/apps/details?id=it.tiwiz.shareitapp\">");
		builder.append(getTheAppString);
		builder.append("</a>");
		
		final String uri = "file://" + app.getPackagePath();
		
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("application/vnd.android.package-archive");
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, app.getAppName() + "-" + sharedWith); 
		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(builder.toString()));
		shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri));

        mContext.startActivity(Intent.createChooser(shareIntent, shareTitle));
		
	}
	
	public static void shareMoreLinks(final Context mContext, final List<App> apps){
		
		final String sharedWith = mContext.getResources().getString(R.string.share_it_app_promo_text);
		final String shareTitle = mContext.getResources().getString(R.string.share_with);
		
		new Runnable(){
			@Override
			public void run() {
				
				StringBuilder builder = new StringBuilder();
				
				for(App app : apps)
					builder.append(createLink(mContext,app));
				
				builder.append(sharedWith);
				
				Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, sharedWith); 
				shareIntent.putExtra(Intent.EXTRA_TEXT, builder.toString());
				
				mContext.startActivity(Intent.createChooser(shareIntent, shareTitle));
			}
			
		}.run();
	}

    public static void shareMoreAPKs(final Context mContext, final List<App> apps){

        ShareZipFile szf = new ShareZipFile(mContext, apps);
        szf.run();
    }


    public static PendingIntent allAndroidZipIntent(Context mContext, String zipPath, int requestID, boolean old){

        if(old == true) return createZipPendingIntent(mContext,zipPath);

        Intent newApi = new Intent(mContext,InvisibleActivity.class);
        newApi.putExtra(ZIP_PATH,zipPath);
        newApi.putExtra(REQUEST_PID,requestID);

        return PendingIntent.getActivity(mContext, 0, newApi, Notification.FLAG_AUTO_CANCEL);
    }

    public static PendingIntent createZipPendingIntent(Context mContext, String zipPath){

        Intent shareIntent = createZipIntent(zipPath);
        return PendingIntent.getActivity(mContext, 0, shareIntent, Notification.FLAG_AUTO_CANCEL);

    }

    public static Intent createZipIntent(String zipPath){

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("application/zip");
        final String uri = "file://" + zipPath;
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri));

        return shareIntent;
    }

    public static PendingIntent createErrorReportIntent(Context mContext, String message, boolean deleted, String trimmedZipName){

        Intent zipResultIntent = new Intent(mContext,ZipResult.class);
        zipResultIntent.putExtra(Intents.ZIP_ERROR_MESSAGE, message);
        zipResultIntent.putExtra(Intents.ZIP_ERROR_DELETE, deleted);
        zipResultIntent.putExtra(Intents.ZIP_ERROR_NAME,trimmedZipName);

        return PendingIntent.getActivity(mContext,0,zipResultIntent,0);
    }

    public static void sendZipErrorResult(Context mContext, String message){

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mContext.getString(R.string.zip_error_report_subject));
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        shareIntent.putExtra(Intent.EXTRA_EMAIL,new String[]{mContext.getString(R.string.zip_mail)});

        mContext.startActivity(Intent.createChooser(shareIntent, mContext.getString(R.string.zip_send_mail)));
    }
}
