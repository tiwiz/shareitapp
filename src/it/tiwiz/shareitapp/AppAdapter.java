package it.tiwiz.shareitapp;

import java.util.ArrayList;
import java.util.List;

import org.holoeverywhere.widget.CheckBox;
import org.holoeverywhere.widget.TextView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

@SuppressLint("DefaultLocale")
public class AppAdapter extends BaseAdapter implements Filterable{
	
	private List<App> data, originalData;
	private Context mContext;
	private static CheckBox[] checkboxes;
	private static boolean[] selected;
	
	public AppAdapter(Context mContext, List<App> data){
		this.mContext = mContext;
		this.data = data;
		this.originalData = data;
		checkboxes = new CheckBox[data.size()];
		selected = new boolean[data.size()];
		for(int i = data.size() -1; i>=0; i--)
			selected[i] = false;
	}
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public App getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void uncheckAll(){

		for(int i = 0; i<data.size(); i++)
			selected[i] = false;
		
		ShareItActivity.selectedApps.clear();
		ShareItActivity.broadcastRefresh();
	}
	
	
	public void clear(){
		checkboxes = new CheckBox[data.size()];
		data.clear();
	}

	@Override
	public View getView(final int position, View contentView, ViewGroup parent) {
		ViewHolder holder;
		if(position > data.size()) return null;
		final App app = data.get(position);
		
		if(contentView == null){
			LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView = li.inflate(R.layout.app_element_layout, parent, false);
			holder = new ViewHolder();
			holder.btnAppInfo = (ImageButton) contentView.findViewById(R.id.btnAppElementAppInfo);
			holder.btnPlayStore = (ImageButton) contentView.findViewById(R.id.btnAppElementPlayStore);
			holder.btnShare = (ImageButton) contentView.findViewById(R.id.btnAppElementShare);
			holder.imgAppIcon = (ImageView) contentView.findViewById(R.id.imgAppIcon);
			holder.txtAppName = (TextView) contentView.findViewById(R.id.txtAppElementName);
			holder.txtAppPackage = (TextView) contentView.findViewById(R.id.txtAppElementPackage);
			holder.chkSelected = (CheckBox) contentView.findViewById(R.id.chkSelected);
			holder.chkSelected.setChecked(selected[position]); //EDIT 10-APR-2013
			holder.imgAppInstallerIcon = (ImageView) contentView.findViewById(R.id.imgAppInstallerIcon);
			checkboxes[position] = holder.chkSelected;
			
			contentView.setTag(holder);
		}else
			holder = (ViewHolder) contentView.getTag();
		
		final View myContentViewLink = contentView;
		//insert data
		holder.txtAppName.setText(app.getAppName());
		holder.txtAppPackage.setText(app.getPackageName());
		holder.imgAppIcon.setImageDrawable(app.getAppIcon());
		boolean isElemSelected = selected[position];
		holder.chkSelected.setChecked(isElemSelected);
		int color;
		
		if(isElemSelected) color = R.color.holo_light_blue;
		else color = R.color.abs__background_holo_light;
		
		RelativeLayout rl = (RelativeLayout) contentView.findViewById(R.id.front);
		rl.setBackgroundColor(mContext.getResources().getColor(color));
		
		holder.imgAppInstallerIcon.setImageDrawable(app.getAppInstallerIcon());
		
		holder.btnPlayStore.setImageDrawable(app.getAppInstallerIcon());
		
		holder.btnPlayStore.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Intents.startPlayStore(mContext, app);
			}	
		});
		
		holder.btnPlayStore.setOnLongClickListener(new OnLongClickListener(){
			@Override
			public boolean onLongClick(View arg0) {
				Intents.startAppOptions(mContext,app);
				return true;
			}});

		holder.btnAppInfo.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intents.openAppInfo(mContext, app.getPackageName());
			}
		});

        holder.btnAppInfo.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intents.uninstallApp(mContext, app.getPackageName());
                return false;
            }
        });

		holder.btnShare.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intents.shareLink(mContext, app);
			}
		});

        holder.btnShare.setOnLongClickListener(new OnLongClickListener(){
            @Override
            public boolean onLongClick(View view) {
                Intents.shareAPK(mContext, app);
                return true;
            }});
		
		holder.chkSelected.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				RelativeLayout rl = (RelativeLayout) myContentViewLink.findViewById(R.id.front);
				boolean isChecked = ((CheckBox)v).isChecked();
				Log.d("DEBUG", "onClick -> isChecked: " + isChecked);
				selected[position] = isChecked;

				if(isChecked == true){
					ShareItActivity.selectedApps.add(app);
					rl.setBackgroundColor(mContext.getResources().getColor(R.color.holo_light_blue));
					if(!UIManager.isCancelSelectionButtonShowing()) UIManager.showCancelSelectionButton();
				}else{
					boolean found = false;
					for(int i = 0; (i<ShareItActivity.selectedApps.size()) && (found == false); i++){
						String tmpPackage = ShareItActivity.selectedApps.get(i).getPackageName();
						if(app.getPackageName().contentEquals(tmpPackage)){
							ShareItActivity.selectedApps.remove(i);
							rl.setBackgroundColor(mContext.getResources().getColor(R.color.abs__background_holo_light));
							found = true;
						}	
					}
					
					if((UIManager.isCancelSelectionButtonShowing() == true) && (ShareItActivity.selectedApps.size() == 0))
						UIManager.hideCancelSelectionButton();
				}
				
			}});
		return contentView;
	}
	
	static class ViewHolder{
		ImageButton btnPlayStore, btnAppInfo, btnShare;
		TextView txtAppName, txtAppPackage;
		CheckBox chkSelected;
		ImageView imgAppIcon, imgAppInstallerIcon;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public Filter getFilter() {
		
		return new Filter(){

			@SuppressLint("DefaultLocale")
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				List<App> filteredApps = new ArrayList<App>();
				
				int size = originalData.size();
				
				if(constraint == null || constraint.length() == 0){
					results.count = size;
					results.values = originalData;
				}else{
					
					constraint = constraint.toString().toLowerCase();
					for(int i = 0; i<size; i++){
						App tmpApp = originalData.get(i);
						if(tmpApp.getAppName().toLowerCase().contains(constraint) || tmpApp.getPackageName().toLowerCase().contains(constraint))
							filteredApps.add(tmpApp);
					}
					
					results.count = filteredApps.size();
					results.values = filteredApps;
				}
				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				data = (List<App>) results.values;
				notifyDataSetChanged();
			}};
	}

}
