package it.tiwiz.shareitapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PackageReceiver extends BroadcastReceiver{
		@Override
	public void onReceive(Context localContext, Intent callerIntent) {
		String action = callerIntent.getAction();
		if(action.contentEquals(Intent.ACTION_PACKAGE_ADDED) || action.contentEquals(Intent.ACTION_PACKAGE_REMOVED))
			ShareItActivity.broadcastRefresh();
	}

}
