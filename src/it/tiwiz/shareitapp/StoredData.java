package it.tiwiz.shareitapp;

import android.content.Context;
import android.content.SharedPreferences;

public class StoredData {
	
	public static final String ROOT_KEY = "it.tiwiz.shareitapp";
    public static final String BACKUP_KEY = ROOT_KEY + "_backup";
	private static final String APPENDIX = "_details";
	private static final String FIRST_TIME_RUN = "first.time";
	
	private static SharedPreferences _getPrefs(Context mContext){
		return mContext.getSharedPreferences(ROOT_KEY, Context.MODE_PRIVATE);
	}
	
	public static void saveInstaller(Context mContext, String packageName, int installer){
		saveInstaller(mContext,packageName,installer,"");
	}
	
	public static void saveInstaller(Context mContext, String packageName, int installer, String details){
		
		SharedPreferences.Editor editor = _getPrefs(mContext).edit();
		editor.putInt(packageName, installer);
		
		String key = packageName + APPENDIX;
		
		editor.putString(key, details);
		editor.commit();

        SIABackup.requestBackup(mContext); //requests backup
		
		ShareItActivity.broadcastRefresh();
		
	}
	
	public static Installer getInstaller(Context mContext, String packageName){
		
		Installer installer = new Installer();
		SharedPreferences prefs = _getPrefs(mContext);
		
		int source = prefs.getInt(packageName, App.NOT_SET);
		
		if(source != App.NOT_SET){
			String key = packageName + APPENDIX;
			String value = prefs.getString(key, "");
			installer.setType(source);
			installer.setDetails(value);
		}
		
		return installer;
	}
	
	public static boolean firstTimeAppRun(Context mContext){
		
		SharedPreferences prefs = _getPrefs(mContext);
		boolean result = prefs.getBoolean(FIRST_TIME_RUN, true);
		
		if(result == true){
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(FIRST_TIME_RUN, false);
			editor.commit();
		}
		
		return result;
	}
}
