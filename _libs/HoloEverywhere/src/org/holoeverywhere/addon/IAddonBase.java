
package org.holoeverywhere.addon;

import org.holoeverywhere.app.Activity;

public abstract class IAddonBase {
    public abstract Activity getActivity();
}
